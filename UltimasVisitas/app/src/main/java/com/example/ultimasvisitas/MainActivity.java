package com.example.ultimasvisitas;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void openDialog (View view) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);

        String information = "Larva encontrada: Sim|Imóvel tratado: Não|Agente responsável: fulano|Data da visita: 17/05/2023|Situação última Visita: Fechada";
        UnaryOperator<String> lastVisitsInfo = last -> (last.replace("|", "\n"));

        dialog.setIcon(android.R.drawable.ic_dialog_info);
        dialog.setTitle("Dados da última visita: ");
        dialog.setMessage(lastVisitsInfo.apply(information));

        //Criar e exibir AlertDialog
        dialog.create();
        dialog.show();
    }
}